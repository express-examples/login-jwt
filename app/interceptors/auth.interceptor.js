const jwt = require('../../utils/jwt');

exports.isLogin = function(req, res, next) {
  try {
    const authHeader = req.headers.authorization.split(" ")[1];
    if (jwt.verify(token)) {
      req.usuario = token.usuario;
      next();
    } else {
      res.status(401).json({error: 'token no autorizado'});
    }
  } catch (error) {
    res.status(500).json({error: 'error con el token'});
  }
}

