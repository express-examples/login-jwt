const db = require('../models');
const crypto = require('crypto');
const jwt = require('../../utils/jwt');

exports.login = async function(req, res) {
  try {
    const {email, password} = req.body;
    const hashPassword = crypto.createHmac('sha256', 'llavedehash')
                               .update(password)
                               .digest('hex');

    const usuario = await db.Usuario.findOne({
                                where: {
                                  email: email,
                                  password: hashPassword
                                },
                                attributes: ['nombres', 'apellidos', 'email', 'dni']
                              });
    if (usuario) {
      //firmar el token jwt
      const token = jwt.sign({usuario});
      //regresar el token jwt
      res.json({token: `Bearer ${token}`});
    } else {
      res.status(401).json({error: 'email o clave invalidas.'});
    }
  } catch(err) {
    res.status(500).json({error: err});
  }
}
