const express = require('express');
const authInterceptor = require('../interceptors/auth.interceptor');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({ title: 'mi pagina' });
});
router.get('/protegido', authInterceptor.isLogin, function(req, res, next) {
  res.json({ title: 'sitio protegido', session: req.session });
});

router.get('/desprotegido', function(req, res, next) {
  res.json({ title: 'sitio desprotegido', session: req.session });
});


module.exports = router;
