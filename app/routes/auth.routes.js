const express = require('express');
const authCtrl = require('../controllers/auth.controller');

const router = express.Router();
/* GET users listing. */
router.post('/login', authCtrl.login );

module.exports = router;
